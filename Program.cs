﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading.Tasks;

namespace AnnoyingBot
{
    public class AnnoyingModule : ModuleBase<SocketCommandContext>
    {
        [Command("late")]
        [Summary("When a cunt is late")]
        public Task SayAsync([Remainder] [Summary("Lmao")] string text)
        {
            if (Program.counted < 7)
            {
                string name = text.Split('>')[0] + ">";
                var emoji = new Emoji("\uD83D\uDE21");
                string message = name + " YOU ARE LATE! " + emoji;
                Console.WriteLine(message);
                return ReplyAsync(message);
            }
            else
            {
                string message = "This is an Emergency Broadcasting Service.The country has been attacked with nuclear weapons.Communications have been severely disrupted, and the number of casualties are not yet known. We shall bring you further information as soon aspossible. Meanwhile, stay tuned to this wavelength, stay calm and stay in your own house." +
                    "Remember there is nothing to be gained by trying to get away.By leaving your homes you could be exposing yourself to greater danger.";
                Console.WriteLine(message);
                return ReplyAsync(message, true);
            }
        }
    }
    public class CommandHandler
    {

        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        public CommandHandler(DiscordSocketClient client, CommandService commands)
        {
            _commands = commands;
            _client = client;
        }

        public async Task InstallCommandsAsync()
        {
            // Hook the MessageReceived event into our command handler
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                            services: null);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a system message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!(message.HasCharPrefix('!', ref argPos) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            // Create a WebSocket-based command context based on the message
            var context = new SocketCommandContext(_client, message);

            // Execute the command with the command context we just
            // created, along with the service provider for precondition checks.

            // Keep in mind that result does not indicate a return value
            // rather an object stating if the command executed successfully.
            if (message.Content.Contains("late"))
            {
                Console.WriteLine("Late again!!");

                Program.counted = 0;
                int i = 0;
                while (true)
                {
                    if (i == 8)
                        break;
                    var result = await _commands.ExecuteAsync(
                        context: context,
                        argPos: argPos,
                        services: null);
                    i++;
                    Program.counted = i;
                }
            }
            else
            {
                var result = await _commands.ExecuteAsync(
                       context: context,
                       argPos: argPos,
                       services: null);
            }
            // Optionally, we may inform the user if the command fails
            // to be executed; however, this may not always be desired,
            // as it may clog up the request queue should a user spam a
            // command.
            // if (!result.IsSuccess)
            // await context.Channel.SendMessageAsync(result.ErrorReason);
        }
    }
    class Program
    {
        private static readonly ulong clips = 700447776743620749;
        public static int counted = 0;
        public static DiscordSocketClient _client;
        private static CommandService _service;
        public static IConfigurationRoot config = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .AddEnvironmentVariables()
              .Build();
        static async Task Main(string[] args)
        {
            _client = new DiscordSocketClient();
            _service = new CommandService();
            _client.Log += _client_Log;
            Console.WriteLine("token supplied: " + config["discord"]);
            await _client.LoginAsync(TokenType.Bot, config["discord"]);
            await _client.StartAsync();
            _client.Ready += () =>
            {
                Console.WriteLine("Bot is connected!");
                return Task.CompletedTask;
            };
            _client.MessageReceived += _client_MessageReceived;
            var c = new CommandHandler(_client, _service);
            await c.InstallCommandsAsync();
            // Block this task until the program is closed.
            await Task.Delay(-1);

        }

        private static Task _client_MessageReceived(SocketMessage arg)
        {
            if (arg.Content.Contains("6xQW3q0M2Qc"))
            {
                Console.WriteLine("Replying with my own video!");
                 arg.DeleteAsync();
                _client.GetGuild(_client.Guilds.FirstOrDefault().Id).GetTextChannel(clips).SendMessageAsync("https://www.youtube.com/watch?v=DOSFMg93l2M&feature=emb_title");
            }
            if(arg.Author.IsBot && arg.Content.Contains("Wartime"))
            {
                arg.DeleteAsync();
            }
            return Task.CompletedTask;
        }

        private static Task _client_Log(LogMessage arg)
        {
            Console.WriteLine(arg.ToString());
            return Task.CompletedTask;
        }
    }
}
